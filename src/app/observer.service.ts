import { OnInit } from "@angular/core";
import { Subject } from 'rxjs';

export class ObserverService {

    observerDS: Subject<String> = new Subject<string>();
}