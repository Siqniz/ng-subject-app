import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ComposerComponent } from './composer/composer.component';
import { RecieverComponent } from './reciever/reciever.component';
import { ObserverService } from './observer.service';

//services


@NgModule({
  declarations: [
    AppComponent,
    ComposerComponent,
    RecieverComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ObserverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
