import { Component, OnInit } from '@angular/core';
import { ObserverService } from '../observer.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-composer',
  templateUrl: './composer.component.html',
  styleUrls: ['./composer.component.css']
})
export class ComposerComponent implements OnInit {

  subjectService:Subject<string>;
  constructor(private obsvc: ObserverService) { }


  ngOnInit() {

  }

  sendToRecieverComponent(data:string){
    this.obsvc.observerDS.next(Date.now().toString())
  }

}
