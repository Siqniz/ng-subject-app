import { Component, OnInit, OnDestroy } from '@angular/core';
import { ObserverService } from '../observer.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-reciever',
  templateUrl: './reciever.component.html',
  styleUrls: ['./reciever.component.css']
})
export class RecieverComponent implements OnInit, OnDestroy {
  subjectSub: Subscription;
  datasource: string[] = []
  constructor(private obsvc: ObserverService) { }

  ngOnInit() {
    this.subjectSub = this.obsvc.observerDS.subscribe((data: string) => {
      this.datasource.push(data);
    });
  }

  ngOnDestroy() {
    this.subjectSub.unsubscribe();
  }
}
